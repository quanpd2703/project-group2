﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class Carts
    {
        [Key]
        public int CartID { get; set; }
        public int CustomerID { get; set; }
        public Customers? Customers { get; set; }
        public ICollection<CartDetails>? CartDetails { get; set; }

    }
}
