﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class Functions
    {
        [Key]
        public int FunctionID { get; set; }
        public string? FunctionName { get; set; }
        public ICollection<UserFunction>? UserFunctions { get; set; }
        public ICollection<FunctionGroup>? FunctionGroups { get; set; }


    }
}
