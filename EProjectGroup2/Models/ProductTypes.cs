﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class ProductTypes
    {
        [Key]
        public int ProductTypeID { get; set; }
        public string? ProductName { get; set; }
        public string? Description { get; set; }
        public ICollection<Products>? Products { get; set; }

    }
}
