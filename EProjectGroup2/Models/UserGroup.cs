﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class UserGroup
    {
        public int UserID { get; set; }
        public Users? Users { get; set; }
        public int GroupID { get; set; }
        public Groups? Groups { get; set; }
    }
}
