﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class Feedbacks
    {
        [Key]
        public int FeedbackID { get; set; }
        public string? FeedbackName { get; set; }
        public string? Description { get; set; }
        public int? ProductID { get; set; }
        public Products? Products { get; set; }

    }
}
