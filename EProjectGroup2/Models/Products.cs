﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class Products
    {
        [Key]
        public int ProductID { get; set; }
        public string? ProductName { get; set; }
        public string? ImagePath { get; set; }
        public string? Description { get; set; }
        public int? Quantity { get; set; }
        public float? Price { get; set; }
        public DateTime? ImportDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public DateTime? ManufacturedDate { get; set; }
        public string? Manufacturer { get; set; }
        public int CategoryID { get; set; }
        public int ProductTypeID { get; set; }
        public Categories? Categories { get; set; }
        public ProductTypes? ProductTypes { get; set; }
        public ICollection<Feedbacks>? Feedbacks { get; set; }
        public ICollection<OrderDetails>? OrderDetails { get; set; }
        public ICollection<CartDetails>? CartDetails { get; set; }

    }
}
