﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class Orders
    {
        [Key]
        public int OrderID { get; set; }
        public string? Message { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? Status { get; set; }
        public int? AddressID { get; set; }
        public int? CustomerID { get; set; }
        public int? UserID { get; set; }
        public ICollection<OrderDetails>? OrderDetails { get; set; }
        public Address? Address { get; set; }
        public Customers? Customers { get; set; }
        public Users? Users { get; set; }

    }
}
