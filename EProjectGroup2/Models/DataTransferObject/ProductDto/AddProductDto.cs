﻿namespace EProjectGroup2.Models.DataTransferObject.ProductDto
{
    public class AddProductDto
    {
        public string? ProductName { get; set; }
        public string? ImagePath { get; set; }
        public IFormFile? ImageFile { get; set; }
        public string? Description { get; set; }
        public int? Quantity { get; set; }
        public float? Price { get; set; }
        public DateTime? ImportDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public DateTime? ManufacturedDate { get; set; }
        public string? Manufacturer { get; set; }
        public int CategoryID { get; set; }
        public int ProductTypeID { get; set; }
    }
}
