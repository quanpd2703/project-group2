﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class OrderDetails
    {
        public int? Quantity { get; set; }
        public float? Discount { get; set; }

        public int ProductID { get; set; }
        public Products? Products { get; set; }
        public int OrderID { get; set; }
        public Orders? Orders { get; set; }
    }
}
