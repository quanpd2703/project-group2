﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class Users
    {
        [Key]
        public int UserID { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public string? FullName { get; set; }
        public string? Email { get; set; } 
        public string? PhoneNumber { get; set; }
        public ICollection<UserGroup>? UserGroups { get; set; }
        public ICollection<UserFunction>? UserFunctions { get; set; }
        public ICollection<Orders>? Orders { get; set; }
        public ICollection<Educations>? Educations { get; set; }


    }
}
