﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class Groups
    {
        [Key]
        public int GroupID { get; set; }
        public string? GroupName { get; set; }
        public ICollection<UserGroup>? UserGroups { get; set; }
        public ICollection<FunctionGroup>? FunctionGroups { get; set; }

    }
}
