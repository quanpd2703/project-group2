﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class Customers
    {
        [Key]
        public int CustomerID { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int? CartID { get; set; }
        public Carts? Carts { get; set; }
        public ICollection<Address>? Address { get; set; }
        public ICollection<Orders>? Orders { get; set; }


    }
}
