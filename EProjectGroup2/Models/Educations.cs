﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class Educations
    {
        [Key]
        public int EducationID { get; set; }
        public string? EducationName { get; set; }
        public string? EducationDescription { get; set; }
        public string? EducationType { get; set; }
        public string? EducationContent { get; set; }
        public DateTime? EducationDate { get; set; }
        public int? UserID { get; set; }
        public Users? Users { get; set; }
    }
}
