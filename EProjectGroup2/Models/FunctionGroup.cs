﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class FunctionGroup
    {
        public int FunctionID { get; set; }
        public Functions? Functions { get; set; }
        public int GroupID { get; set; }
        public Groups? Groups { get; set; }
    }
}
