﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class Address
    {
        [Key]
        public int AddressID { get; set; }
        public string? AddressName { get; set; }
        public int? CustomerID { get; set; }
        public Customers ? Customers { get; set; }
        public IEnumerable<Orders>? Orders { get; set; }

    }
}
