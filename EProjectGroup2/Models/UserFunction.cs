﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class UserFunction
    {
        public int UserID { get; set; }
        public Users? Users { get; set; }
        public int FunctionID { get; set; }
        public Functions? Functions { get; set; }
    }
}
