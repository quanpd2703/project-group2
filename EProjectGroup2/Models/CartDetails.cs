﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class CartDetails
    {
        public int? Quantity { get; set; }
        public int ProductID { get; set; }
        public Products? Products { get; set; }
        public int CartID { get; set; }
        public Carts? Carts { get; set; }

    }
}
