﻿using EProjectGroup2.Models;
using Microsoft.EntityFrameworkCore;

namespace EProjectGroup2.DataContext
{
    public class ClinicManagementDataContext: DbContext
    {
        public ClinicManagementDataContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Users> Users { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Carts> Carts { get; set; }
        public DbSet<Categories> Categories { get; set; }
        public DbSet<Customers> Customers { get; set; }
        public DbSet<Feedbacks> Feedbacks { get; set; }
        public DbSet<Educations> Educations { get; set; }
        public DbSet<Functions> Functions { get; set; }
        public DbSet<Groups> Groups { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<Products> Products { get; set; }
        public DbSet<OrderDetails> OrderDetails { get; set; }
        public DbSet<ProductTypes> ProductTypes { get; set; }
        public DbSet<UserFunction> UserFunction { get; set; }
        public DbSet<FunctionGroup> FunctionGroup { get; set; }
        public DbSet<UserGroup> UserGroup { get; set; }
        public DbSet<CartDetails> CartDetails { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            #region Set Key
            builder.Entity<Products>()
                .HasKey(p => p.ProductID);

            builder.Entity<Categories>()
                .HasKey(c => c.CategoryID);

            builder.Entity<ProductTypes>()
                .HasKey(pt => pt.ProductTypeID);

            builder.Entity<Feedbacks>()
                .HasKey(c => c.FeedbackID);

            builder.Entity<Orders>()
                .HasKey(p => p.OrderID);

            builder.Entity<Address>()
                .HasKey(a => a.AddressID);

            builder.Entity<Customers>()
                .HasKey(a => a.CustomerID);

            builder.Entity<Carts>()
                .HasKey(a => a.CartID);

            builder.Entity<Functions>()
                .HasKey(a => a.FunctionID);

            builder.Entity<Groups>()
                .HasKey(a => a.GroupID);

            builder.Entity<Users>()
                .HasKey(a => a.UserID);

            builder.Entity<Educations>()
                .HasKey(a => a.EducationID);

            builder.Entity<CartDetails>().HasKey(cd => new { cd.CartID, cd.ProductID });
            builder.Entity<UserFunction>().HasKey(cd => new { cd.UserID, cd.FunctionID });
            builder.Entity<UserGroup>().HasKey(cd => new { cd.UserID, cd.GroupID });
            builder.Entity<OrderDetails>().HasKey(cd => new { cd.OrderID, cd.ProductID });
            builder.Entity<FunctionGroup>().HasKey(cd => new { cd.FunctionID, cd.GroupID });
            #endregion

            #region Relationship
            // One-to-many relationship between Categories and Products
            builder.Entity<Products>()
                .HasOne(p => p.Categories)
                .WithMany(pt => pt.Products)
                .HasForeignKey(p => p.CategoryID);

            // One-to-many relationship between ProductTypes and Products
            builder.Entity<Products>()
                .HasOne(p => p.ProductTypes)
                .WithMany(pt => pt.Products)
                .HasForeignKey(p => p.ProductTypeID);

            // One-to-many relationship between Feedbacks and Products
            builder.Entity<Feedbacks>()
                .HasOne(p => p.Products)
                .WithMany(f => f.Feedbacks)
                .HasForeignKey(p => p.ProductID);

            // One-to-many relationship between Products and OrderDetails
            builder.Entity<OrderDetails>()
                .HasOne(od => od.Products)
                .WithMany(p => p.OrderDetails)
                .HasForeignKey(od => od.ProductID);

            builder.Entity<Products>()
                .HasMany(p => p.OrderDetails)
                .WithOne(od => od.Products)
                .HasForeignKey(od => od.ProductID);

            // One-to-many relationship between Products and CartDetails
            builder.Entity<CartDetails>()
                .HasOne(cd => cd.Products)
                .WithMany(p => p.CartDetails)
                .HasForeignKey(cd => cd.ProductID);

            builder.Entity<Products>()
                .HasMany(p => p.CartDetails)
                .WithOne(cd => cd.Products)
                .HasForeignKey(cd => cd.ProductID);


            // Relationships for Customers table
            builder.Entity<Customers>()
                .HasOne(c => c.Carts)
                .WithOne(c => c.Customers)
                .HasForeignKey<Carts>(c => c.CustomerID);

            builder.Entity<Customers>()
                .HasMany(c => c.Address)
                .WithOne(a => a.Customers)
                .HasForeignKey(a => a.CustomerID);

            builder.Entity<Customers>()
               .HasMany(c => c.Orders)
               .WithOne(a => a.Customers)
               .HasForeignKey(a => a.CustomerID);


            // Relationships for OrderDetails table
            builder.Entity<OrderDetails>()
                .HasOne(od => od.Products)
                .WithMany(p => p.OrderDetails)
                .HasForeignKey(od => od.ProductID);

            builder.Entity<OrderDetails>()
                .HasOne(od => od.Orders)
                .WithMany(o => o.OrderDetails)
                .HasForeignKey(od => od.OrderID);


            // Relationships for Order table

            builder.Entity<Orders>()
                .HasMany(o => o.OrderDetails)
                .WithOne(od => od.Orders)
                .HasForeignKey(od => od.OrderID);

            builder.Entity<Orders>()
                .HasOne(o => o.Address)
                .WithMany(o => o.Orders)
                .HasForeignKey(o => o.AddressID);

            builder.Entity<Orders>()
                .HasOne(o => o.Users)
                .WithMany(o => o.Orders)
                .HasForeignKey(o => o.UserID);

            // Relationships for Address table

            builder.Entity<Address>()
                .HasOne(a => a.Customers)
                .WithMany(c => c.Address)
                .HasForeignKey(a => a.CustomerID);
            builder.Entity<Address>()
                .HasMany(a => a.Orders)
                .WithOne(o => o.Address)
                .HasForeignKey(o => o.AddressID);


            // Relationships for Education table

            builder.Entity<Educations>()
                .HasOne(o => o.Users)
                .WithMany(o => o.Educations)
                .HasForeignKey(o => o.UserID);

            // Relationships for Permission table

            builder.Entity<UserGroup>()
                .HasOne(a => a.Users)
                .WithMany(x => x.UserGroups)
                .HasForeignKey(x => x.UserID);

            builder.Entity<UserGroup>()
                .HasOne(a => a.Groups)
                .WithMany(x => x.UserGroups)
                .HasForeignKey(x => x.GroupID);

            builder.Entity<UserFunction>()
                .HasOne(a => a.Users)
                .WithMany(x => x.UserFunctions)
                .HasForeignKey(x => x.UserID);

            builder.Entity<UserFunction>()
                .HasOne(a => a.Functions)
                .WithMany(x => x.UserFunctions)
                .HasForeignKey(x => x.FunctionID);

            builder.Entity<FunctionGroup>()
                .HasOne(x => x.Functions)
                .WithMany(x => x.FunctionGroups)
                .HasForeignKey(x => x.FunctionID);

            builder.Entity<FunctionGroup>()
               .HasOne(x => x.Groups)
               .WithMany(x => x.FunctionGroups)
               .HasForeignKey(x => x.GroupID);

            // Relationships for Permission table
            builder.Entity<Carts>()
                .HasMany(x => x.CartDetails)
                .WithOne(x => x.Carts)
                .HasForeignKey(x => x.CartID);

            builder.Entity<Carts>()
                .HasOne(x => x.Customers)
                .WithOne(x => x.Carts)
                .HasForeignKey<Customers>(x => x.CartID);

            #endregion

            #region HasData Categories
            builder.Entity<Categories>().HasData(
                    new Categories
                    {
                        CategoryID = 1,
                        CategoryName = "Pharmaceutical Drug",
                        Description = "A medication (also called medicament, medicine, pharmaceutical drug, medicinal drug or simply drug) is a drug used to diagnose, cure, treat, or prevent disease. Drug therapy (pharmacotherapy) is an important part of the medical field and relies on the science of pharmacology for continual advancement and on pharmacy for appropriate management.",
                    },
                    new Categories
                    {
                        CategoryID = 2,
                        CategoryName = "Medical Equipment",
                        Description = "A medical device is any device intended to be used for medical purposes. Significant potential for hazards are inherent when using a device for medical purposes and thus medical devices must be proved safe and effective with reasonable assurance before regulating governments allow marketing of the device in their country.",
                    });
            #endregion

            #region HasData Product Type
            builder.Entity<ProductTypes>().HasData(
                    new ProductTypes
                    {
                        ProductTypeID = 1,
                        ProductName = "Antipyretics",
                        Description = "Antipyretics block or reverse fever's cytokine-mediated rise in core temperature, but do not affect body temperature in the afebrile state.",
                    },
                    new ProductTypes
                    {
                        ProductTypeID = 2,
                        ProductName = "Analgesic",
                        Description = "An analgesic drug, also called simply an analgesic (American English), analgaesic (British English), pain reliever, or painkiller, is any member of the group of drugs used to achieve relief from pain (that is, analgesia or pain management).",
                    },
                    new ProductTypes
                    {
                        ProductTypeID = 3,
                        ProductName = "Medical Device",
                        Description = "A medical device is any device intended to be used for medical purposes. Significant potential for hazards are inherent when using a device for medical purposes and thus medical devices must be proved safe and effective with reasonable assurance before regulating governments allow marketing of the device in their country. As a general rule, as the associated risk of the device increases the amount of testing required to establish safety and efficacy also increases. Further, as associated risk increases the potential benefit to the patient must also increase.",
                    }
                    );
            #endregion

            #region HasData Product
            builder.Entity<Products>().HasData(
                new Products
                {
                    ProductID = 1,
                    ProductName = "Paradon",
                    Quantity = 100,
                    Price = 10000,
                    Description = "Panadol medicine contains paracetamol which is an active pain reliever and fever reducer. The drug is effective in reducing fever and alleviating mild to moderate pain symptoms",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = "default.png",
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "DMP",
                    CategoryID = 1,
                    ProductTypeID = 1,
                },
                new Products
                {
                    ProductID = 2,
                    ProductName = "Lipitor",
                    Quantity = 100,
                    Price = 10000,
                    Description = "a medication used to lower cholesterol levels in the blood.",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = "default.png",
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "DMP",
                    CategoryID = 1,
                    ProductTypeID = 2,
                },
                new Products
                {
                    ProductID = 3,
                    ProductName = "Advil",
                    Quantity = 100,
                    Price = 10000,
                    Description = "a nonsteroidal anti-inflammatory drug (NSAID) used to relieve pain and reduce inflammation.",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = "default.png",
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "DMP",
                    CategoryID = 1,
                    ProductTypeID = 2,
                },
                new Products
                {
                    ProductID = 4,
                    ProductName = "Synthroid ",
                    Quantity = 100,
                    Price = 10000,
                    Description = "a medication used to treat hypothyroidism by replacing or supplementing the body's natural thyroid hormone.",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = "default.png",
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "DMP",
                    CategoryID = 1,
                    ProductTypeID = 2,
                },
                new Products
                {
                    ProductID = 5,
                    ProductName = "Metformin ",
                    Quantity = 100,
                    Price = 10000,
                    Description = "a medication used to treat type 2 diabetes by lowering blood sugar levels.",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = "default.png",
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "DMP",
                    CategoryID = 1,
                    ProductTypeID = 2,
                },
                new Products
                {
                    ProductID = 6,
                    ProductName = "Syringe",
                    Quantity = 100,
                    Price = 10000,
                    Description = "a hollow, cylinder-shaped piece of equipment used for sucking liquid out of something or pushing liquid into something, especially one with a needle that can be put under the skin and used to inject drugs, remove small amounts of blood, etc.",
                    ExpireDate = null,
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = "default.png",
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "DMP",
                    CategoryID = 2,
                    ProductTypeID = 3,
                },
                new Products
                {
                    ProductID = 7,
                    ProductName = "Thermometer",
                    Quantity = 100,
                    Price = 10000,
                    Description = "a device used for measuring temperature, especially of the air or in a person's body.",
                    ExpireDate = null,
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = "default.png",
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "DMP",
                    CategoryID = 2,
                    ProductTypeID = 3,
                },
                 new Products
                 {
                     ProductID = 8,
                     ProductName = "Pill",
                     Quantity = 100,
                     Price = 10000,
                     Description = "a small solid piece of medicine that a person swallows without chewing",
                     ExpireDate = null,
                     ImportDate = new DateTime(2022, 12, 12),
                     ImagePath = "default.png",
                     ManufacturedDate = new DateTime(2023, 02, 12),
                     Manufacturer = "DMP",
                     CategoryID = 2,
                     ProductTypeID = 3,
                 },
                  new Products
                  {
                      ProductID = 9,
                      ProductName = "Tweezers",
                      Quantity = 100,
                      Price = 10000,
                      Description = "a small piece of equipment made of two narrow strips of metal joined at one end. It is used to pull out hairs or to pick up small objects by pressing the two strips of metal together with the fingers",
                      ExpireDate = null,
                      ImportDate = new DateTime(2022, 12, 12),
                      ImagePath = "default.png",
                      ManufacturedDate = new DateTime(2023, 02, 12),
                      Manufacturer = "DMP",
                      CategoryID = 2,
                      ProductTypeID = 3,
                  },
                  new Products
                  {
                      ProductID = 10,
                      ProductName = "Stethoscope",
                      Quantity = 100,
                      Price = 10000,
                      Description = "a piece of medical equipment that doctors use to listen to your heart and lungs",
                      ExpireDate = null,
                      ImportDate = new DateTime(2022, 12, 12),
                      ImagePath = "default.png",
                      ManufacturedDate = new DateTime(2023, 02, 12),
                      Manufacturer = "DMP",
                      CategoryID = 2,
                      ProductTypeID = 3,
                  }
                );
            #endregion

            #region HasData Education
            builder.Entity<Educations>().HasData(
                    new Educations
                    {
                        EducationID = 1,
                        EducationName = "Surgical Robotics Continue Rapid Expansion in 2022",
                        EducationDescription = " Both major and smaller players in surgical robotics jockeyed for market share in 2022 by expanding indications and providing greater access and flexibility, either alone or by forging partnerships.",
                        EducationContent = "Meanwhile, Titan Medical and Medtronic announced in September a limited development program, building on the successful completion of the June 2020 development and license agreement between the two companies. The definitive agreement includes a preclinical collaboration to evaluate the performance of various instruments and cameras in gynecological procedures. ",
                        EducationDate = new DateTime(2022, 12, 12),
                        EducationType = "Education"
                    }
                    );
            #endregion

            #region HasData Customer
            builder.Entity<Customers>().HasData(
                    new Customers
                    {
                        CustomerID = 1,
                        Name="Tommy",
                        CartID = 1,
                        DateOfBirth= new DateTime(2022, 12, 12),
                        Email="abc@abc",
                        UserName="tommy",
                        Password="tommy",
                        PhoneNumber="123",
                    }
                    );
            #endregion

            #region HasData Cart
            builder.Entity<Carts>().HasData(
                    new Carts
                    {
                        CustomerID = 1,
                        CartID = 1
                    }
                    );
            #endregion

            #region HasData Orders
            builder.Entity<Orders>().HasData(
                    new Orders
                    {
                        CustomerID = 1,
                        OrderID = 1,
                        AddressID = 1,
                        CreatedDate = DateTime.Now,
                        Message="None",
                        Status=true,
                    }
                    );
            ;
            #endregion

            #region HasData Address
            builder.Entity<Address>().HasData(
                    new Address
                    {
                        AddressID = 1,
                        AddressName = "Hanoi",
                        CustomerID = 1,
                    }
                    );
            #endregion

            #region HasData Users
            builder.Entity<Users>().HasData(
                    new Users
                    {
                        UserID=1,
                        UserName = "admin",
                        Password= "admin",
                        Email = "admin@admin",
                        FullName="admin",
                        PhoneNumber="1234"
                    }
                    );
            #endregion
        }

    }

}
