const toggleMenu = document.querySelectorAll("[role='toggle-btn']");
const drawer = document.querySelector(".drawer-wrapper");
toggleMenu.forEach((btn) => {
  btn.onclick = () => {
    drawer.classList.toggle("active");
  };
});

const switchBtn = document.querySelectorAll("[role='switch']");
const wrapperBox = document.querySelector("[role='wrapperBox']");
switchBtn.forEach((btn) => {
  btn.onclick = () => {
    wrapperBox.classList.toggle("isLogin");
  };
});

const subBtn = document.querySelectorAll("button.action-quantity");
subBtn.forEach((btn) => {
  const inputElm = document
    .querySelector(`#btn-wrapper:has([type='${btn.type}'])`)
    .querySelector("input");
  btn.onclick = (e) => {
    e.stopPropagation();
    const role = btn.getAttribute("role");
    if (role === "add-quantity") {
      inputElm.value++;
    } else {
      inputElm.value != 1 && inputElm.value--;
    }
  };
});
