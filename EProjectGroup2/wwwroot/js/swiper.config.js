const swiper = new Swiper(".swiper", {
    loop: true,
    autoplay: {
        delay: 2000,
    },
    effect: "fade",
    fadeEffect: {
        crossFade: true,
    },

    pagination: {
        el: ".swiper-pagination",
    },

    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});

console.log("???")