﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EProjectGroup2.DataContext;
using EProjectGroup2.Models;

namespace EProjectGroup2.Controllers
{
    public class UserFunctionsController : Controller
    {
        private readonly ClinicManagementDataContext _context;

        public UserFunctionsController(ClinicManagementDataContext context)
        {
            _context = context;
        }

        // GET: UserFunctions
        public async Task<IActionResult> Index()
        {
            var clinicManagementDataContext = _context.UserFunction.Include(u => u.Functions).Include(u => u.Users);
            return View(await clinicManagementDataContext.ToListAsync());
        }

        // GET: UserFunctions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.UserFunction == null)
            {
                return NotFound();
            }

            var userFunction = await _context.UserFunction
                .Include(u => u.Functions)
                .Include(u => u.Users)
                .FirstOrDefaultAsync(m => m.UserID == id);
            if (userFunction == null)
            {
                return NotFound();
            }

            return View(userFunction);
        }

        // GET: UserFunctions/Create
        public IActionResult Create()
        {
            ViewData["FunctionID"] = new SelectList(_context.Functions, "FunctionID", "FunctionID");
            ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserID");
            return View();
        }

        // POST: UserFunctions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserID,FunctionID")] UserFunction userFunction)
        {
            if (ModelState.IsValid)
            {
                _context.Add(userFunction);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["FunctionID"] = new SelectList(_context.Functions, "FunctionID", "FunctionID", userFunction.FunctionID);
            ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserID", userFunction.UserID);
            return View(userFunction);
        }

        // GET: UserFunctions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.UserFunction == null)
            {
                return NotFound();
            }

            var userFunction = await _context.UserFunction.FindAsync(id);
            if (userFunction == null)
            {
                return NotFound();
            }
            ViewData["FunctionID"] = new SelectList(_context.Functions, "FunctionID", "FunctionID", userFunction.FunctionID);
            ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserID", userFunction.UserID);
            return View(userFunction);
        }

        // POST: UserFunctions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("UserID,FunctionID")] UserFunction userFunction)
        {
            if (id != userFunction.UserID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(userFunction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserFunctionExists(userFunction.UserID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["FunctionID"] = new SelectList(_context.Functions, "FunctionID", "FunctionID", userFunction.FunctionID);
            ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserID", userFunction.UserID);
            return View(userFunction);
        }

        // GET: UserFunctions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.UserFunction == null)
            {
                return NotFound();
            }

            var userFunction = await _context.UserFunction
                .Include(u => u.Functions)
                .Include(u => u.Users)
                .FirstOrDefaultAsync(m => m.UserID == id);
            if (userFunction == null)
            {
                return NotFound();
            }

            return View(userFunction);
        }

        // POST: UserFunctions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.UserFunction == null)
            {
                return Problem("Entity set 'ClinicManagementDataContext.UserFunction'  is null.");
            }
            var userFunction = await _context.UserFunction.FindAsync(id);
            if (userFunction != null)
            {
                _context.UserFunction.Remove(userFunction);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserFunctionExists(int id)
        {
          return (_context.UserFunction?.Any(e => e.UserID == id)).GetValueOrDefault();
        }
    }
}
