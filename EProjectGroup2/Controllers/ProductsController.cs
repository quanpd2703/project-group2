﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EProjectGroup2.DataContext;
using EProjectGroup2.Models;
using EProjectGroup2.Models.DataTransferObject.ProductDto;

namespace EProjectGroup2.Controllers
{
    public class ProductsController : Controller
    {
        private readonly ClinicManagementDataContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;

        public ProductsController(ClinicManagementDataContext context, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            _hostEnvironment = hostEnvironment;
        }

        // GET: Products
        public async Task<IActionResult> Index()
        {
            var clinicManagementDataContext = _context.Products.Include(p => p.Categories).Include(p => p.ProductTypes);
            return View(await clinicManagementDataContext.ToListAsync());
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var products = await _context.Products
                .Include(p => p.Categories)
                .Include(p => p.ProductTypes)
                .FirstOrDefaultAsync(m => m.ProductID == id);
            if (products == null)
            {
                return NotFound();
            }
            return View(products);
        }

        // GET: Products/Create
        public IActionResult Create()
        {
            ViewData["CategoryID"] = new SelectList(_context.Categories, "CategoryID", "CategoryName");
            ViewData["ProductTypeID"] = new SelectList(_context.ProductTypes, "ProductTypeID", "ProductName");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AddProductDto products)
        {
            if (ModelState.IsValid)
            {
                if(products.ImageFile == null)
                {
                    products.ImagePath = "";
                }
                else
                {
                    //Save image to wwwroot/image
                    string wwwRootPath = _hostEnvironment.WebRootPath;
                    string fileName = Path.GetFileNameWithoutExtension(products.ImageFile.FileName);
                    string extension = Path.GetExtension(products.ImageFile.FileName);
                    products.ImagePath = fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                    string path = Path.Combine(wwwRootPath + "/Image/", fileName);
                    using (var fileStream = new FileStream(path, FileMode.Create))
                    {
                        await products.ImageFile.CopyToAsync(fileStream);
                    }
                }
                var modelProduct = new Products
                {
                    ProductName = products.ProductName,
                    Description = products.Description,
                    Quantity = products.Quantity,
                    Price = products.Price,
                    ImagePath = products.ImagePath,
                    ImportDate = products.ImportDate,
                    ExpireDate = products.ExpireDate,
                    ManufacturedDate = products.ManufacturedDate,
                    Manufacturer = products.Manufacturer,
                    CategoryID = products.CategoryID,
                    ProductTypeID = products.ProductTypeID
                };
                _context.Add(modelProduct);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryID"] = new SelectList(_context.Categories, "CategoryID", "CategoryName", products.CategoryID);
            ViewData["ProductTypeID"] = new SelectList(_context.ProductTypes, "ProductTypeID", "ProductName", products.ProductTypeID);
            return View(products);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var products = await _context.Products.FindAsync(id);
            if (products == null)
            {
                return NotFound();
            }
            
            var modelProduct = new UpdateProductDto
            {
                ProductID = products.ProductID,
                ProductName = products.ProductName,
                Description = products.Description,
                Quantity = products.Quantity,
                Price = products.Price,
                ImagePath = products.ImagePath,
                ImportDate = products.ImportDate,
                ExpireDate = products.ExpireDate,
                ManufacturedDate = products.ManufacturedDate,
                Manufacturer = products.Manufacturer,
                CategoryID = products.CategoryID,
                ProductTypeID = products.ProductTypeID
            };
            ViewData["CategoryID"] = new SelectList(_context.Categories, "CategoryID", "CategoryName", modelProduct.CategoryID);
            ViewData["ProductTypeID"] = new SelectList(_context.ProductTypes, "ProductTypeID", "ProductName", modelProduct.ProductTypeID);
            return View(modelProduct);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, UpdateProductDto products)
        {
            if (id != products.ProductID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (products.ImageFile == null)
                    {
                        products.ImagePath = "";
                    }
                    else
                    {
                        //Save image to wwwroot/image
                        string wwwRootPath = _hostEnvironment.WebRootPath;
                        string fileName = Path.GetFileNameWithoutExtension(products.ImageFile.FileName);
                        string extension = Path.GetExtension(products.ImageFile.FileName);
                        products.ImagePath = fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                        string path = Path.Combine(wwwRootPath + "/Image/", fileName);
                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            await products.ImageFile.CopyToAsync(fileStream);
                        }
                    }
                    var modelProduct = new Products
                    {
                        ProductID = products.ProductID,
                        ProductName = products.ProductName,
                        Description = products.Description,
                        Quantity = products.Quantity,
                        Price = products.Price,
                        ImagePath = products.ImagePath,
                        ImportDate = products.ImportDate,
                        ExpireDate = products.ExpireDate,
                        ManufacturedDate = products.ManufacturedDate,
                        Manufacturer = products.Manufacturer,
                        CategoryID = products.CategoryID,
                        ProductTypeID = products.ProductTypeID
                    };
                    _context.Update(modelProduct);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductsExists(products.ProductID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryID"] = new SelectList(_context.Categories, "CategoryID", "CategoryName", products.CategoryID);
            ViewData["ProductTypeID"] = new SelectList(_context.ProductTypes, "ProductTypeID", "ProductTypeName", products.ProductTypeID);
            return View(products);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var products = await _context.Products
                .Include(p => p.Categories)
                .Include(p => p.ProductTypes)
                .FirstOrDefaultAsync(m => m.ProductID == id);
            if (products == null)
            {
                return NotFound();
            }

            return View(products);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Products == null)
            {
                return Problem("Entity set 'ClinicManagementDataContext.Products'  is null.");
            }
            var products = await _context.Products.FindAsync(id);
            if (products != null)
            {
                _context.Products.Remove(products);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductsExists(int id)
        {
          return (_context.Products?.Any(e => e.ProductID == id)).GetValueOrDefault();
        }
    }
}
