using EProjectGroup2.DataContext;
using EProjectGroup2.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace EProjectGroup2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ClinicManagementDataContext _context;


        public HomeController(ILogger<HomeController> logger, ClinicManagementDataContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var pharmacy = await _context.Products.Include(p => p.Categories).Include(p => p.ProductTypes).Where(p => p.CategoryID == 1).ToListAsync();
            var medical = await _context.Products.Include(p => p.Categories).Include(p => p.ProductTypes).Where(p => p.CategoryID == 2).ToListAsync();
            List<Products> pha = pharmacy;
            List<Products> med = medical;
            ViewData["Pharmacy"] = pha;
            ViewData["medical"] = med;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> Scientific()
        {
            var clinicManagementDataContext = _context.Products.Include(p => p.Categories).Include(p => p.ProductTypes);
            var data = clinicManagementDataContext.Where(x => x.CategoryID == 2);
            return View(await data.ToListAsync());
        }

        public async Task<IActionResult> Medical()
        {
            var clinicManagementDataContext = _context.Products.Include(p => p.Categories).Include(p => p.ProductTypes).Where(p => p.CategoryID == 1);
            return View(await clinicManagementDataContext.ToListAsync());
        }

        public async Task<IActionResult> Educations()
        {
            var clinicManagementDataContext = _context.Educations.Include(e => e.Users);
            return View(await clinicManagementDataContext.ToListAsync());
        }
        
		public IActionResult Contact()
		{
			return View();
		}
	
        public async Task<IActionResult> Orders(int? orderId)
        {
            var orders = _context.Orders
                .Include(o => o.OrderDetails)
                    .ThenInclude(od => od.Products)
                .Include(o => o.Customers)
                .Include(x=>x.Address)
                .ToList();

            if (orderId != null)
            {
                orders = orders.Where(o => o.OrderID == orderId).ToList();
            }

            return View(orders);
        }
    }
}