﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EProjectGroup2.DataContext;
using EProjectGroup2.Models;

namespace EProjectGroup2.Controllers
{
    public class CartDetailsController : Controller
    {
        private readonly ClinicManagementDataContext _context;  

        public CartDetailsController(ClinicManagementDataContext context)
        {
            _context = context;
        }

        // GET: CartDetails
        public async Task<IActionResult> Index()
        {
            var cart = GetOrCreateCartForCurrentCustomer();
            var clinicManagementDataContext = _context.CartDetails.Include(c => c.Carts).Include(c => c.Products).Where(x=>x.CartID==cart.CartID);
            return View(await clinicManagementDataContext.ToListAsync());
        }

        // GET: CartDetails/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.CartDetails == null)
            {
                return NotFound();
            }

            var cartDetails = await _context.CartDetails
                .Include(c => c.Carts)
                .Include(c => c.Products)
                .FirstOrDefaultAsync(m => m.CartID == id);
            if (cartDetails == null)
            {
                return NotFound();
            }

            return View(cartDetails);
        }

        // GET: CartDetails/Create
        public IActionResult Create()
        {
            ViewData["CartID"] = new SelectList(_context.Carts, "CartID", "CartID");
            ViewData["ProductID"] = new SelectList(_context.Products, "ProductID", "ProductID");
            return View();
        }

        // POST: CartDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductID,CartID")] CartDetails cartDetails)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cartDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CartID"] = new SelectList(_context.Carts, "CartID", "CartID", cartDetails.CartID);
            ViewData["ProductID"] = new SelectList(_context.Products, "ProductID", "ProductID", cartDetails.ProductID);
            return View(cartDetails);
        }

        // GET: CartDetails/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.CartDetails == null)
            {
                return NotFound();
            }

            var cartDetails = await _context.CartDetails.FindAsync(id);
            if (cartDetails == null)
            {
                return NotFound();
            }
            ViewData["CartID"] = new SelectList(_context.Carts, "CartID", "CartID", cartDetails.CartID);
            ViewData["ProductID"] = new SelectList(_context.Products, "ProductID", "ProductID", cartDetails.ProductID);
            return View(cartDetails);
        }

        // POST: CartDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProductID,CartID")] CartDetails cartDetails)
        {
            if (id != cartDetails.CartID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cartDetails);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CartDetailsExists(cartDetails.CartID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CartID"] = new SelectList(_context.Carts, "CartID", "CartID", cartDetails.CartID);
            ViewData["ProductID"] = new SelectList(_context.Products, "ProductID", "ProductID", cartDetails.ProductID);
            return View(cartDetails);
        }

        // GET: CartDetails/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.CartDetails == null)
            {
                return NotFound();
            }

            var cartDetails = await _context.CartDetails
                .Include(c => c.Carts)
                .Include(c => c.Products)
                .FirstOrDefaultAsync(m => m.CartID == id);
            if (cartDetails == null)
            {
                return NotFound();
            }

            return View(cartDetails);
        }

        // POST: CartDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.CartDetails == null)
            {
                return Problem("Entity set 'ClinicManagementDataContext.CartDetails'  is null.");
            }
            var cartDetails = await _context.CartDetails.FindAsync(id);
            if (cartDetails != null)
            {
                _context.CartDetails.Remove(cartDetails);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CartDetailsExists(int id)
        {
          return (_context.CartDetails?.Any(e => e.CartID == id)).GetValueOrDefault();
        }


        public IActionResult AddToCart(int productId, int quantity)
        {
            var product = _context.Products.FirstOrDefault(p => p.ProductID == productId);
            if (product == null)
            {
                return NotFound();
            }

            var cart = GetOrCreateCartForCurrentCustomer();

            if (cart == null) return RedirectToAction("LoginCustomer", "Customers");

            var cartDetail = cart.CartDetails.FirstOrDefault(cd => cd.ProductID == productId);
            if (cartDetail == null)
            {
                cartDetail = new CartDetails
                {
                    CartID = cart.CartID,
                    ProductID = productId,
                    Quantity = quantity
                };
                _context.CartDetails.Add(cartDetail);
            }
            else
            {
                cartDetail.Quantity += quantity;
            }
            if (cart.CustomerID == null)
            {
                cart.CustomerID = int.Parse(HttpContext.Request.Cookies["CustomerId"]);
            }

            _context.SaveChanges();

            return RedirectToAction("Index" ,new { cartId = cart.CartID });
        }

        public IActionResult RemoveFromCart(int productId)
        {
            var cart = GetOrCreateCartForCurrentCustomer();

            var cartDetail = cart.CartDetails.FirstOrDefault(cd => cd.ProductID == productId);
            if (cartDetail != null)
            {
                _context.CartDetails.Remove(cartDetail);
                _context.SaveChanges();
            }

            return RedirectToAction("Index", new { cartId = cart.CartID });
        }

        private Carts GetOrCreateCartForCurrentCustomer()
        {
            if (HttpContext.Request.Cookies.TryGetValue("CustomerId", out string customerIdStr) && int.TryParse(customerIdStr, out int customerId))
            {
                var cart = _context.Carts
                    .Include(c => c.CartDetails)
                    .ThenInclude(cd => cd.Products)
                    .FirstOrDefault(c => c.CustomerID == customerId);

                if (cart == null)
                {
                    cart = new Carts { CustomerID = customerId };
                    _context.Carts.Add(cart);
                    _context.SaveChanges();
                }

                return cart;
            }

            // CustomerId cookie not found or invalid
            return null;
        }
        public IActionResult AddToOrderDetails(int[] selectedProducts)
        {
            // Validate inputs
            if (selectedProducts == null || selectedProducts.Length == 0)
            {
                ModelState.AddModelError(string.Empty, "Please select at least one product to add to order details.");
                return View("Index", _context.CartDetails.Include(cd => cd.Products));
            }

            // Get the cart for the current customer
            var cart = GetOrCreateCartForCurrentCustomer();

            // Update the cart's customer ID if it hasn't been set yet
            if (cart.CustomerID == null)
            {
                cart.CustomerID = int.Parse(HttpContext.Request.Cookies["CustomerId"]);
            }

            // Create a new order and set its customer ID
            var order = new Orders
            {
                CustomerID = cart.CustomerID,
                CreatedDate = DateTime.Now,
            };
            _context.Orders.Add(order);
            _context.SaveChanges();

            // Create new order details for each selected product
            var orderDetails = new List<OrderDetails>();
            foreach (var productId in selectedProducts)
            {
                var cartDetail = cart.CartDetails.FirstOrDefault(cd => cd.ProductID == productId);
                if (cartDetail != null)
                {
                    orderDetails.Add(new OrderDetails
                    {
                        ProductID = cartDetail.ProductID,
                        Quantity = cartDetail.Quantity,
                        Discount = null,
                        OrderID = order.OrderID
                    });

                    _context.CartDetails.Remove(cartDetail);
                }
            }

            _context.OrderDetails.AddRange(orderDetails);
            _context.SaveChanges();

            return RedirectToAction("Index", "OrderDetails");
        }


    }
}
