﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EProjectGroup2.DataContext;
using EProjectGroup2.Models;

namespace EProjectGroup2.Controllers
{
    public class UserGroupsController : Controller
    {
        private readonly ClinicManagementDataContext _context;

        public UserGroupsController(ClinicManagementDataContext context)
        {
            _context = context;
        }

        // GET: UserGroups
        public async Task<IActionResult> Index()
        {
            var clinicManagementDataContext = _context.UserGroup.Include(u => u.Groups).Include(u => u.Users);
            return View(await clinicManagementDataContext.ToListAsync());
        }

        // GET: UserGroups/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.UserGroup == null)
            {
                return NotFound();
            }

            var userGroup = await _context.UserGroup
                .Include(u => u.Groups)
                .Include(u => u.Users)
                .FirstOrDefaultAsync(m => m.UserID == id);
            if (userGroup == null)
            {
                return NotFound();
            }

            return View(userGroup);
        }

        // GET: UserGroups/Create
        public IActionResult Create()
        {
            ViewData["GroupID"] = new SelectList(_context.Groups, "GroupID", "GroupID");
            ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserID");
            return View();
        }

        // POST: UserGroups/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserID,GroupID")] UserGroup userGroup)
        {
            if (ModelState.IsValid)
            {
                _context.Add(userGroup);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupID"] = new SelectList(_context.Groups, "GroupID", "GroupID", userGroup.GroupID);
            ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserID", userGroup.UserID);
            return View(userGroup);
        }

        // GET: UserGroups/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.UserGroup == null)
            {
                return NotFound();
            }

            var userGroup = await _context.UserGroup.FindAsync(id);
            if (userGroup == null)
            {
                return NotFound();
            }
            ViewData["GroupID"] = new SelectList(_context.Groups, "GroupID", "GroupID", userGroup.GroupID);
            ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserID", userGroup.UserID);
            return View(userGroup);
        }

        // POST: UserGroups/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("UserID,GroupID")] UserGroup userGroup)
        {
            if (id != userGroup.UserID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(userGroup);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserGroupExists(userGroup.UserID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupID"] = new SelectList(_context.Groups, "GroupID", "GroupID", userGroup.GroupID);
            ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserID", userGroup.UserID);
            return View(userGroup);
        }

        // GET: UserGroups/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.UserGroup == null)
            {
                return NotFound();
            }

            var userGroup = await _context.UserGroup
                .Include(u => u.Groups)
                .Include(u => u.Users)
                .FirstOrDefaultAsync(m => m.UserID == id);
            if (userGroup == null)
            {
                return NotFound();
            }

            return View(userGroup);
        }

        // POST: UserGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.UserGroup == null)
            {
                return Problem("Entity set 'ClinicManagementDataContext.UserGroup'  is null.");
            }
            var userGroup = await _context.UserGroup.FindAsync(id);
            if (userGroup != null)
            {
                _context.UserGroup.Remove(userGroup);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserGroupExists(int id)
        {
          return (_context.UserGroup?.Any(e => e.UserID == id)).GetValueOrDefault();
        }
    }
}
