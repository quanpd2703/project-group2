﻿using EProjectGroup2.DataContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace EProjectGroup2.Controllers
{
    public class AdminController : Controller
    {
		private readonly ClinicManagementDataContext _context;

		public AdminController(ClinicManagementDataContext context)
		{
			_context = context;
		}
		public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Users()
        {
            return _context.Users != null ?
						  View(await _context.Users.ToListAsync()) :
						  Problem("Entity set 'ClinicManagementDataContext.Users'  is null.");
		}

		[HttpPost]
		public async Task<IActionResult> Users(string txtSearch)
		{
			//if (HttpContext.Request.Cookies["UsrName"] == "Admin")
			//{
			//	return RedirectToAction("Login", "Users");
			//}

			if (txtSearch == null || _context.Users == null)
			{
				return NotFound();
			}
			var users = from m in _context.Users
						select m;

			if (!String.IsNullOrEmpty(txtSearch))
			{
				users = users.Where(s => s.UserName!.Contains(txtSearch));
			}

			return View(await users.ToListAsync());
		}

		public async Task<IActionResult> Products()
		{
			var clinicManagementDataContext = _context.Products.Include(p => p.Categories).Include(p => p.ProductTypes);
            ViewData["CategoryID"] = new SelectList(_context.Categories, "CategoryID", "CategoryName");
            ViewData["ProductTypeID"] = new SelectList(_context.ProductTypes, "ProductTypeID", "ProductName");
            return View(await clinicManagementDataContext.ToListAsync());
		}

		[HttpPost]
		public async Task<IActionResult> Products(string txtSearch)
		{
			//if (HttpContext.Request.Cookies["UsrName"] == "Admin")
			//{
			//	return RedirectToAction("Login", "Users");
			//}

			if (txtSearch == null || _context.Products == null)
			{
				return NotFound();
			}
			var Products = from m in _context.Products
						select m;

			if (!String.IsNullOrEmpty(txtSearch))
			{
				Products = Products.Where(s => s.ProductName!.Contains(txtSearch));
			}

			return View(await Products.ToListAsync());
		}

		public async Task<IActionResult> Educations()
		{
			var clinicManagementDataContext = _context.Educations.Include(e => e.Users);
			return View(await clinicManagementDataContext.ToListAsync());
		}

		[HttpPost]
		public async Task<IActionResult> Educations(string txtSearch)
		{
			//if (HttpContext.Request.Cookies["UsrName"] == "Admin")
			//{
			//	return RedirectToAction("Login", "Users");
			//}

			if (txtSearch == null || _context.Educations == null)
			{
				return NotFound();
			}
			var Educations = from m in _context.Educations
						select m;

			if (!String.IsNullOrEmpty(txtSearch))
			{
				Educations = Educations.Where(s => s.EducationName!.Contains(txtSearch));
			}

			return View(await Educations.ToListAsync());
		}

		// GET: Feedbacks
		public async Task<IActionResult> FeedBacks()
		{
			var clinicManagementDataContext = _context.Feedbacks.Include(f => f.Products);
			return View(await clinicManagementDataContext.ToListAsync());
		}

		[HttpPost]
		public async Task<IActionResult> FeedBacks(string txtSearch)
		{
			//if (HttpContext.Request.Cookies["UsrName"] == "Admin")
			//{
			//	return RedirectToAction("Login", "Users");
			//}

			if (txtSearch == null || _context.Feedbacks == null)
			{
				return NotFound();
			}
			var FeedBacks = from m in _context.Feedbacks
						select m;

			if (!String.IsNullOrEmpty(txtSearch))
			{
				FeedBacks = FeedBacks.Where(s => s.FeedbackName!.Contains(txtSearch));
			}

			return View(await FeedBacks.ToListAsync());
		}

		// GET: Orders
		public async Task<IActionResult> Orders()
		{
			var clinicManagementDataContext = _context.Orders.Include(o => o.Address).Include(o => o.Customers).Include(o => o.Users);
			ViewData["AddressID"] = new SelectList(_context.Address, "AddressID", "AddressName");
			ViewData["CustomerID"] = new SelectList(_context.Customers, "CustomerID", "Name");
			ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserName");
			return View(await clinicManagementDataContext.ToListAsync());
		}

		[HttpPost]
		public async Task<IActionResult> Orders(string txtSearch)
		{
			//if (HttpContext.Request.Cookies["UsrName"] == "Admin")
			//{
			//	return RedirectToAction("Login", "Users");
			//}

			if (txtSearch == null || _context.Orders == null)
			{
				return NotFound();
			}
			var Orders = from m in _context.Orders
							select m;

			if (!String.IsNullOrEmpty(txtSearch))
			{
				Orders = Orders.Where(s => s.Message!.Contains(txtSearch));
			}

			return View(await Orders.ToListAsync());
		}

        // GET: Customers
        public async Task<IActionResult> Custommers()
        {
            var clinicManagementDataContext = _context.Customers.Include(c => c.Carts);
            return View(await clinicManagementDataContext.ToListAsync());
        }

        [HttpPost]
        public async Task<IActionResult> Custommers(string txtSearch)
        {
            //if (HttpContext.Request.Cookies["UsrName"] == "Admin")
            //{
            //	return RedirectToAction("Login", "Users");
            //}

            if (txtSearch == null || _context.Customers == null)
            {
                return NotFound();
            }
            var Custommers = from m in _context.Customers
                             select m;

            if (!String.IsNullOrEmpty(txtSearch))
            {
                Custommers = Custommers.Where(s => s.Name!.Contains(txtSearch));
            }

            return View(await Custommers.ToListAsync());
        }
    }
}
