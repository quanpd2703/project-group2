﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EProjectGroup2.DataContext;
using EProjectGroup2.Models;

namespace EProjectGroup2.Controllers
{
    public class FunctionGroupsController : Controller
    {
        private readonly ClinicManagementDataContext _context;

        public FunctionGroupsController(ClinicManagementDataContext context)
        {
            _context = context;
        }

        // GET: FunctionGroups
        public async Task<IActionResult> Index()
        {
            var clinicManagementDataContext = _context.FunctionGroup.Include(f => f.Functions).Include(f => f.Groups);
            return View(await clinicManagementDataContext.ToListAsync());
        }

        // GET: FunctionGroups/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.FunctionGroup == null)
            {
                return NotFound();
            }

            var functionGroup = await _context.FunctionGroup
                .Include(f => f.Functions)
                .Include(f => f.Groups)
                .FirstOrDefaultAsync(m => m.FunctionID == id);
            if (functionGroup == null)
            {
                return NotFound();
            }

            return View(functionGroup);
        }

        // GET: FunctionGroups/Create
        public IActionResult Create()
        {
            ViewData["FunctionID"] = new SelectList(_context.Functions, "FunctionID", "FunctionID");
            ViewData["GroupID"] = new SelectList(_context.Groups, "GroupID", "GroupID");
            return View();
        }

        // POST: FunctionGroups/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FunctionID,GroupID")] FunctionGroup functionGroup)
        {
            if (ModelState.IsValid)
            {
                _context.Add(functionGroup);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["FunctionID"] = new SelectList(_context.Functions, "FunctionID", "FunctionID", functionGroup.FunctionID);
            ViewData["GroupID"] = new SelectList(_context.Groups, "GroupID", "GroupID", functionGroup.GroupID);
            return View(functionGroup);
        }

        // GET: FunctionGroups/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.FunctionGroup == null)
            {
                return NotFound();
            }

            var functionGroup = await _context.FunctionGroup.FindAsync(id);
            if (functionGroup == null)
            {
                return NotFound();
            }
            ViewData["FunctionID"] = new SelectList(_context.Functions, "FunctionID", "FunctionID", functionGroup.FunctionID);
            ViewData["GroupID"] = new SelectList(_context.Groups, "GroupID", "GroupID", functionGroup.GroupID);
            return View(functionGroup);
        }

        // POST: FunctionGroups/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FunctionID,GroupID")] FunctionGroup functionGroup)
        {
            if (id != functionGroup.FunctionID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(functionGroup);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FunctionGroupExists(functionGroup.FunctionID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["FunctionID"] = new SelectList(_context.Functions, "FunctionID", "FunctionID", functionGroup.FunctionID);
            ViewData["GroupID"] = new SelectList(_context.Groups, "GroupID", "GroupID", functionGroup.GroupID);
            return View(functionGroup);
        }

        // GET: FunctionGroups/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.FunctionGroup == null)
            {
                return NotFound();
            }

            var functionGroup = await _context.FunctionGroup
                .Include(f => f.Functions)
                .Include(f => f.Groups)
                .FirstOrDefaultAsync(m => m.FunctionID == id);
            if (functionGroup == null)
            {
                return NotFound();
            }

            return View(functionGroup);
        }

        // POST: FunctionGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.FunctionGroup == null)
            {
                return Problem("Entity set 'ClinicManagementDataContext.FunctionGroup'  is null.");
            }
            var functionGroup = await _context.FunctionGroup.FindAsync(id);
            if (functionGroup != null)
            {
                _context.FunctionGroup.Remove(functionGroup);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FunctionGroupExists(int id)
        {
          return (_context.FunctionGroup?.Any(e => e.FunctionID == id)).GetValueOrDefault();
        }
    }
}
