﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EProjectGroup2.DataContext;
using EProjectGroup2.Models;

namespace EProjectGroup2.Controllers
{
    public class EducationsController : Controller
    {
        private readonly ClinicManagementDataContext _context;

        public EducationsController(ClinicManagementDataContext context)
        {
            _context = context;
        }

        // GET: Educations
        public async Task<IActionResult> Index()
        {
            var clinicManagementDataContext = _context.Educations.Include(e => e.Users);
            return View(await clinicManagementDataContext.ToListAsync());
        }

        // GET: Educations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Educations == null)
            {
                return NotFound();
            }

            var educations = await _context.Educations
                .Include(e => e.Users)
                .FirstOrDefaultAsync(m => m.EducationID == id);
            if (educations == null)
            {
                return NotFound();
            }

            return View(educations);
        }

        // GET: Educations/Create
        public IActionResult Create()
        {
            ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserID");
            return View();
        }

        // POST: Educations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EducationID,EducationName,EducationDescription,EducationType,EducationContent,EducationDate,UserID")] Educations educations)
        {
            if (ModelState.IsValid)
            {
                _context.Add(educations);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserID", educations.UserID);
            return View(educations);
        }

        // GET: Educations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Educations == null)
            {
                return NotFound();
            }

            var educations = await _context.Educations.FindAsync(id);
            if (educations == null)
            {
                return NotFound();
            }
            ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserID", educations.UserID);
            return View(educations);
        }

        // POST: Educations/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EducationID,EducationName,EducationDescription,EducationType,EducationContent,EducationDate,UserID")] Educations educations)
        {
            if (id != educations.EducationID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(educations);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EducationsExists(educations.EducationID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserID"] = new SelectList(_context.Users, "UserID", "UserID", educations.UserID);
            return View(educations);
        }

        // GET: Educations/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Educations == null)
            {
                return NotFound();
            }

            var educations = await _context.Educations
                .Include(e => e.Users)
                .FirstOrDefaultAsync(m => m.EducationID == id);
            if (educations == null)
            {
                return NotFound();
            }

            return View(educations);
        }

        // POST: Educations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Educations == null)
            {
                return Problem("Entity set 'ClinicManagementDataContext.Educations'  is null.");
            }
            var educations = await _context.Educations.FindAsync(id);
            if (educations != null)
            {
                _context.Educations.Remove(educations);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EducationsExists(int id)
        {
          return (_context.Educations?.Any(e => e.EducationID == id)).GetValueOrDefault();
        }
    }
}
