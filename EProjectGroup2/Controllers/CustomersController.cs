﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EProjectGroup2.DataContext;
using EProjectGroup2.Models;

namespace EProjectGroup2.Controllers
{
    public class CustomersController : Controller
    {
        private readonly ClinicManagementDataContext _context;

        public CustomersController(ClinicManagementDataContext context)
        {
            _context = context;
        }

        // GET: Customers
        public async Task<IActionResult> Index()
        {
            var clinicManagementDataContext = _context.Customers.Include(c => c.Carts);
            return View(await clinicManagementDataContext.ToListAsync());
        }

        //GET: Login Custommer
        public IActionResult LoginCustomer()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> LoginCustomer(Customers data)
        {
            Customers? usr = await _context.Customers.FirstOrDefaultAsync(Customers => (Customers.Email == data.Email || Customers.UserName == data.UserName) && Customers.Password == data.Password);
            if (usr != null)
            {
                //ViewBag.username = string.Format("Successfully logged-in", usr.UserName);
                CookieOptions cookieOptions = new();
                HttpContext.Response.Cookies.Append("UsrName", usr.UserName, cookieOptions);
                HttpContext.Response.Cookies.Append("CustomerId", usr.CustomerID.ToString(), cookieOptions);
                HttpContext.Response.Cookies.Append("CustomerName", usr.Name.ToString(), cookieOptions);
                cookieOptions.Expires = new DateTimeOffset(DateTime.Now.AddDays(3));
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //ViewBag.username = string.Format("Login Failed", data.UserName);
                return View();
            }
        }

        // GET: Customers/Details/5
        public async Task<IActionResult> Details(int? id)
        {

            if (id == null || _context.Customers == null)
            {
                return NotFound();
            }

            var customers = await _context.Customers
                .Include(c => c.Carts)
                .FirstOrDefaultAsync(m => m.CustomerID == id);
            if (customers == null)
            {
                return NotFound();
            }
            var address = await _context.Address.Where(a => a.CustomerID == id).ToListAsync();
            List<Address> addresses = address;
            ViewData["AddressName"] = addresses;
            return View(customers);
        }

       
        public async Task<IActionResult> DetailOrder()
        {
            var id = int.Parse(HttpContext.Request.Cookies["CustomerId"]);
            var orderDetails = await _context.OrderDetails
                .Include(o => o.Orders)
                .Include(o => o.Products)
                .Where(o => o.Orders.CustomerID == id)
                .OrderByDescending(o => o.OrderID)
                .ToListAsync();
            var address = await _context.Address.FirstOrDefaultAsync(a => a.CustomerID == id);

            ViewData["AddressID"] = address.AddressName;
            return View(orderDetails);
        }

        // GET: Customers/Create
        public IActionResult Create()
        {
            ViewData["CartID"] = new SelectList(_context.Carts, "CartID", "CartID");
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CustomerID,UserName,Password,Name,Email,PhoneNumber,DateOfBirth,CartID")] Customers customers, [Bind("confirmPassword")] string Confirm)
        {
            if (ModelState.IsValid)
            {
                if (Confirm != null)
                {
                    if (customers.Password == Confirm)
                    {
                        _context.Add(customers);
                        await _context.SaveChangesAsync();

                        var cart = new Carts
                        {
                            CustomerID = customers.CustomerID,
                        };

                        _context.Carts.Add(cart);
                        _context.SaveChanges();

                        customers.CartID = cart.CartID;
                        _context.SaveChanges();
                        return RedirectToAction(nameof(LoginCustomer));
                    }
                }
                else if (HttpContext.Request.Cookies["UsrName"] == "Admin" && HttpContext.Request.Cookies["CustomerName"] == null)
                {
                    _context.Add(customers);
                    await _context.SaveChangesAsync();

                    var cart = new Carts
                    {
                        CustomerID = customers.CustomerID,
                    };

                    _context.Carts.Add(cart);
                    _context.SaveChanges();

                    customers.CartID = cart.CartID;
                    _context.SaveChanges();
                    return RedirectToAction(nameof(LoginCustomer));
                }
            }
            ViewData["CartID"] = new SelectList(_context.Carts, "CartID", "CartID", customers.CartID);
			string? myCookieValue = HttpContext.Request.Cookies["UsrName"];
			if (myCookieValue == "Admin")
			{
                return View(customers);
			}
            return RedirectToAction("Index", "Home");
		}

        // GET: Customers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Customers == null)
            {
                return NotFound();
            }

            var customers = await _context.Customers.FindAsync(id);
            if (customers == null)
            {
                return NotFound();
            }
            ViewData["CartID"] = new SelectList(_context.Carts, "CartID", "CartID", customers.CartID);
            return View(customers);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CustomerID,UserName,Password,Name,Email,PhoneNumber,DateOfBirth,CartID")] Customers customers)
        {
            if (id != customers.CustomerID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(customers);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomersExists(customers.CustomerID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CartID"] = new SelectList(_context.Carts, "CartID", "CartID", customers.CartID);
            return View(customers);
        }

        // GET: Customers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Customers == null)
            {
                return NotFound();
            }

            var customers = await _context.Customers
                .Include(c => c.Carts)
                .FirstOrDefaultAsync(m => m.CustomerID == id);
            if (customers == null)
            {
                return NotFound();
            }

            return View(customers);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Customers == null)
            {
                return Problem("Entity set 'ClinicManagementDataContext.Customers'  is null.");
            }
            var customers = await _context.Customers.FindAsync(id);
            if (customers != null)
            {
                _context.Customers.Remove(customers);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CustomersExists(int id)
        {
          return (_context.Customers?.Any(e => e.CustomerID == id)).GetValueOrDefault();
        }
    }
}
