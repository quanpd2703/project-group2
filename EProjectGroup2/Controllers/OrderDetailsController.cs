﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EProjectGroup2.DataContext;
using EProjectGroup2.Models;

namespace EProjectGroup2.Controllers
{
    public class OrderDetailsController : Controller
    {
        private readonly ClinicManagementDataContext _context;

        public OrderDetailsController(ClinicManagementDataContext context)
        {
            _context = context;
        }

        // GET: OrderDetails
        public async Task<IActionResult> Index()
        {
            var cart = GetOrCreateCartForCurrentCustomer();

            var orderDetails = await _context.OrderDetails
                .Include(o => o.Orders)
                .Include(o => o.Products)
                .Where(o => o.Orders.CustomerID == cart.CustomerID)
                .OrderByDescending(o => o.OrderID)
                .ToListAsync();

            var groupedOrderDetails = orderDetails.GroupBy(o => o.OrderID).ToList();
            if (groupedOrderDetails == null)
            {
                return RedirectToAction("Index","Home");
            }
            return View(groupedOrderDetails);

        }

        private Orders GetOrCreateCartForCurrentCustomer()
        {
            if (HttpContext.Request.Cookies.TryGetValue("CustomerId", out string customerIdStr) && int.TryParse(customerIdStr, out int customerId))
            {
                var cart = _context.Orders
                    .Include(c => c.OrderDetails)
                    .ThenInclude(cd => cd.Products)
                    .FirstOrDefault(c => c.CustomerID == customerId);

                return cart;
            }

            // CustomerId cookie not found or invalid
            return null;
        }
        [HttpPost]
        public IActionResult CreateOrder(int[] selectedOrders)
        {
            var cart = GetOrCreateCartForCurrentCustomer();
            var address = _context.Address.FirstOrDefault(a => a.AddressID == 1);
            var user = _context.Users.FirstOrDefault(x => x.UserID == 1);
            var orderDetails = new List<OrderDetails>();

            foreach (var orderId in selectedOrders)
            {
                var details = _context.OrderDetails
                    .Include(od => od.Products)
                    .Where(od => od.OrderID == orderId);

                foreach (var detail in details)
                {
                    orderDetails.Add(new OrderDetails
                    {
                        Products = detail.Products,
                        Quantity = detail.Quantity
                    });
                }
            }

            var order = new Orders
            {
                Message = "New order",
                CreatedDate = DateTime.Now,
                Status = false,
                AddressID = address.AddressID,
                CustomerID = cart.CustomerID,
                UserID = user.UserID,
                OrderDetails = orderDetails
            };

            _context.Orders.Add(order);
            _context.SaveChanges();

            return RedirectToAction("Orders", "Home", new { orderId = order.OrderID });
        }




        // GET: OrderDetails/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.OrderDetails == null)
            {
                return NotFound();
            }

            var orderDetails = await _context.OrderDetails
                .Include(o => o.Orders)
                .Include(o => o.Products)
                .FirstOrDefaultAsync(m => m.OrderID == id);
            if (orderDetails == null)
            {
                return NotFound();
            }

            return View(orderDetails);
        }

        // GET: OrderDetails/Create
        public IActionResult Create()
        {
            ViewData["OrderID"] = new SelectList(_context.Orders, "OrderID", "OrderID");
            ViewData["ProductID"] = new SelectList(_context.Products, "ProductID", "ProductID");
            return View();
        }

        // POST: OrderDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Quantity,Discount,ProductID,OrderID")] OrderDetails orderDetails)
        {
            if (ModelState.IsValid)
            {
                _context.Add(orderDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["OrderID"] = new SelectList(_context.Orders, "OrderID", "OrderID", orderDetails.OrderID);
            ViewData["ProductID"] = new SelectList(_context.Products, "ProductID", "ProductID", orderDetails.ProductID);
            return View(orderDetails);
        }

        // GET: OrderDetails/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.OrderDetails == null)
            {
                return NotFound();
            }

            var orderDetails = await _context.OrderDetails.FindAsync(id);
            if (orderDetails == null)
            {
                return NotFound();
            }
            ViewData["OrderID"] = new SelectList(_context.Orders, "OrderID", "OrderID", orderDetails.OrderID);
            ViewData["ProductID"] = new SelectList(_context.Products, "ProductID", "ProductID", orderDetails.ProductID);
            return View(orderDetails);
        }

        // POST: OrderDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Quantity,Discount,ProductID,OrderID")] OrderDetails orderDetails)
        {
            if (id != orderDetails.OrderID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(orderDetails);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderDetailsExists(orderDetails.OrderID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["OrderID"] = new SelectList(_context.Orders, "OrderID", "OrderID", orderDetails.OrderID);
            ViewData["ProductID"] = new SelectList(_context.Products, "ProductID", "ProductID", orderDetails.ProductID);
            return View(orderDetails);
        }

        // GET: OrderDetails/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.OrderDetails == null)
            {
                return NotFound();
            }

            var orderDetails = await _context.OrderDetails
                .Include(o => o.Orders)
                .Include(o => o.Products)
                .FirstOrDefaultAsync(m => m.OrderID == id);
            if (orderDetails == null)
            {
                return NotFound();
            }

            return View(orderDetails);
        }

        // POST: OrderDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.OrderDetails == null)
            {
                return Problem("Entity set 'ClinicManagementDataContext.OrderDetails'  is null.");
            }
            var orderDetails = await _context.OrderDetails.FindAsync(id);
            if (orderDetails != null)
            {
                _context.OrderDetails.Remove(orderDetails);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderDetailsExists(int id)
        {
          return (_context.OrderDetails?.Any(e => e.OrderID == id)).GetValueOrDefault();
        }
    }
}
