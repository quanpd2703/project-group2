/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './Pages/**/*.cshtml',
        './Views/**/*.cshtml'
    ],
    theme: {
        extend: {
            colors: {
                primary: "#25292f",
                secondary: "#eb8909",
            },
        },
    },
    plugins: [],
}

