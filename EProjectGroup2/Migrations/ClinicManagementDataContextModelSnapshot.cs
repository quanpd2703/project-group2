﻿// <auto-generated />
using System;
using EProjectGroup2.DataContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace EProjectGroup2.Migrations
{
    [DbContext(typeof(ClinicManagementDataContext))]
    partial class ClinicManagementDataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.15")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("EProjectGroup2.Models.Address", b =>
                {
                    b.Property<int>("AddressID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("AddressID"), 1L, 1);

                    b.Property<string>("AddressName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("CustomerID")
                        .HasColumnType("int");

                    b.HasKey("AddressID");

                    b.HasIndex("CustomerID");

                    b.ToTable("Address");

                    b.HasData(
                        new
                        {
                            AddressID = 1,
                            AddressName = "Hanoi",
                            CustomerID = 1
                        });
                });

            modelBuilder.Entity("EProjectGroup2.Models.CartDetails", b =>
                {
                    b.Property<int>("CartID")
                        .HasColumnType("int");

                    b.Property<int>("ProductID")
                        .HasColumnType("int");

                    b.Property<int?>("Quantity")
                        .HasColumnType("int");

                    b.HasKey("CartID", "ProductID");

                    b.HasIndex("ProductID");

                    b.ToTable("CartDetails");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Carts", b =>
                {
                    b.Property<int>("CartID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("CartID"), 1L, 1);

                    b.Property<int>("CustomerID")
                        .HasColumnType("int");

                    b.HasKey("CartID");

                    b.ToTable("Carts");

                    b.HasData(
                        new
                        {
                            CartID = 1,
                            CustomerID = 1
                        });
                });

            modelBuilder.Entity("EProjectGroup2.Models.Categories", b =>
                {
                    b.Property<int>("CategoryID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("CategoryID"), 1L, 1);

                    b.Property<string>("CategoryName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("CategoryID");

                    b.ToTable("Categories");

                    b.HasData(
                        new
                        {
                            CategoryID = 1,
                            CategoryName = "Pharmaceutical Drug",
                            Description = "A medication (also called medicament, medicine, pharmaceutical drug, medicinal drug or simply drug) is a drug used to diagnose, cure, treat, or prevent disease. Drug therapy (pharmacotherapy) is an important part of the medical field and relies on the science of pharmacology for continual advancement and on pharmacy for appropriate management."
                        },
                        new
                        {
                            CategoryID = 2,
                            CategoryName = "Medical Equipment",
                            Description = "A medical device is any device intended to be used for medical purposes. Significant potential for hazards are inherent when using a device for medical purposes and thus medical devices must be proved safe and effective with reasonable assurance before regulating governments allow marketing of the device in their country."
                        });
                });

            modelBuilder.Entity("EProjectGroup2.Models.Customers", b =>
                {
                    b.Property<int>("CustomerID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("CustomerID"), 1L, 1);

                    b.Property<int?>("CartID")
                        .HasColumnType("int");

                    b.Property<DateTime>("DateOfBirth")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Password")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("CustomerID");

                    b.HasIndex("CartID")
                        .IsUnique()
                        .HasFilter("[CartID] IS NOT NULL");

                    b.ToTable("Customers");

                    b.HasData(
                        new
                        {
                            CustomerID = 1,
                            CartID = 1,
                            DateOfBirth = new DateTime(2022, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Email = "abc@abc",
                            Name = "Tommy",
                            Password = "tommy",
                            PhoneNumber = "123",
                            UserName = "tommy"
                        });
                });

            modelBuilder.Entity("EProjectGroup2.Models.Educations", b =>
                {
                    b.Property<int>("EducationID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("EducationID"), 1L, 1);

                    b.Property<string>("EducationContent")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("EducationDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("EducationDescription")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("EducationName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("EducationType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("UserID")
                        .HasColumnType("int");

                    b.HasKey("EducationID");

                    b.HasIndex("UserID");

                    b.ToTable("Educations");

                    b.HasData(
                        new
                        {
                            EducationID = 1,
                            EducationContent = "Meanwhile, Titan Medical and Medtronic announced in September a limited development program, building on the successful completion of the June 2020 development and license agreement between the two companies. The definitive agreement includes a preclinical collaboration to evaluate the performance of various instruments and cameras in gynecological procedures. ",
                            EducationDate = new DateTime(2022, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            EducationDescription = " Both major and smaller players in surgical robotics jockeyed for market share in 2022 by expanding indications and providing greater access and flexibility, either alone or by forging partnerships.",
                            EducationName = "Surgical Robotics Continue Rapid Expansion in 2022",
                            EducationType = "Education"
                        });
                });

            modelBuilder.Entity("EProjectGroup2.Models.Feedbacks", b =>
                {
                    b.Property<int>("FeedbackID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("FeedbackID"), 1L, 1);

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FeedbackName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("ProductID")
                        .HasColumnType("int");

                    b.HasKey("FeedbackID");

                    b.HasIndex("ProductID");

                    b.ToTable("Feedbacks");
                });

            modelBuilder.Entity("EProjectGroup2.Models.FunctionGroup", b =>
                {
                    b.Property<int>("FunctionID")
                        .HasColumnType("int");

                    b.Property<int>("GroupID")
                        .HasColumnType("int");

                    b.HasKey("FunctionID", "GroupID");

                    b.HasIndex("GroupID");

                    b.ToTable("FunctionGroup");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Functions", b =>
                {
                    b.Property<int>("FunctionID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("FunctionID"), 1L, 1);

                    b.Property<string>("FunctionName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("FunctionID");

                    b.ToTable("Functions");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Groups", b =>
                {
                    b.Property<int>("GroupID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("GroupID"), 1L, 1);

                    b.Property<string>("GroupName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("GroupID");

                    b.ToTable("Groups");
                });

            modelBuilder.Entity("EProjectGroup2.Models.OrderDetails", b =>
                {
                    b.Property<int>("OrderID")
                        .HasColumnType("int");

                    b.Property<int>("ProductID")
                        .HasColumnType("int");

                    b.Property<float?>("Discount")
                        .HasColumnType("real");

                    b.Property<int?>("Quantity")
                        .HasColumnType("int");

                    b.HasKey("OrderID", "ProductID");

                    b.HasIndex("ProductID");

                    b.ToTable("OrderDetails");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Orders", b =>
                {
                    b.Property<int>("OrderID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("OrderID"), 1L, 1);

                    b.Property<int?>("AddressID")
                        .HasColumnType("int");

                    b.Property<DateTime?>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<int?>("CustomerID")
                        .HasColumnType("int");

                    b.Property<string>("Message")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool?>("Status")
                        .HasColumnType("bit");

                    b.Property<int?>("UserID")
                        .HasColumnType("int");

                    b.HasKey("OrderID");

                    b.HasIndex("AddressID");

                    b.HasIndex("CustomerID");

                    b.HasIndex("UserID");

                    b.ToTable("Orders");

                    b.HasData(
                        new
                        {
                            OrderID = 1,
                            AddressID = 1,
                            CreatedDate = new DateTime(2023, 4, 2, 23, 54, 4, 912, DateTimeKind.Local).AddTicks(2843),
                            CustomerID = 1,
                            Message = "None",
                            Status = true
                        });
                });

            modelBuilder.Entity("EProjectGroup2.Models.Products", b =>
                {
                    b.Property<int>("ProductID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("ProductID"), 1L, 1);

                    b.Property<int>("CategoryID")
                        .HasColumnType("int");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("ExpireDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("ImagePath")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("ImportDate")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("ManufacturedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Manufacturer")
                        .HasColumnType("nvarchar(max)");

                    b.Property<float?>("Price")
                        .HasColumnType("real");

                    b.Property<string>("ProductName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ProductTypeID")
                        .HasColumnType("int");

                    b.Property<int?>("Quantity")
                        .HasColumnType("int");

                    b.HasKey("ProductID");

                    b.HasIndex("CategoryID");

                    b.HasIndex("ProductTypeID");

                    b.ToTable("Products");

                    b.HasData(
                        new
                        {
                            ProductID = 1,
                            CategoryID = 1,
                            Description = "Panadol medicine contains paracetamol which is an active pain reliever and fever reducer. The drug is effective in reducing fever and alleviating mild to moderate pain symptoms",
                            ExpireDate = new DateTime(2023, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ImagePath = "default.png",
                            ImportDate = new DateTime(2022, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ManufacturedDate = new DateTime(2023, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Manufacturer = "DMP",
                            Price = 10000f,
                            ProductName = "Paradon",
                            ProductTypeID = 1,
                            Quantity = 100
                        },
                        new
                        {
                            ProductID = 2,
                            CategoryID = 1,
                            Description = "a medication used to lower cholesterol levels in the blood.",
                            ExpireDate = new DateTime(2023, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ImagePath = "default.png",
                            ImportDate = new DateTime(2022, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ManufacturedDate = new DateTime(2023, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Manufacturer = "DMP",
                            Price = 10000f,
                            ProductName = "Lipitor",
                            ProductTypeID = 2,
                            Quantity = 100
                        },
                        new
                        {
                            ProductID = 3,
                            CategoryID = 1,
                            Description = "a nonsteroidal anti-inflammatory drug (NSAID) used to relieve pain and reduce inflammation.",
                            ExpireDate = new DateTime(2023, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ImagePath = "default.png",
                            ImportDate = new DateTime(2022, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ManufacturedDate = new DateTime(2023, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Manufacturer = "DMP",
                            Price = 10000f,
                            ProductName = "Advil",
                            ProductTypeID = 2,
                            Quantity = 100
                        },
                        new
                        {
                            ProductID = 4,
                            CategoryID = 1,
                            Description = "a medication used to treat hypothyroidism by replacing or supplementing the body's natural thyroid hormone.",
                            ExpireDate = new DateTime(2023, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ImagePath = "default.png",
                            ImportDate = new DateTime(2022, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ManufacturedDate = new DateTime(2023, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Manufacturer = "DMP",
                            Price = 10000f,
                            ProductName = "Synthroid ",
                            ProductTypeID = 2,
                            Quantity = 100
                        },
                        new
                        {
                            ProductID = 5,
                            CategoryID = 1,
                            Description = "a medication used to treat type 2 diabetes by lowering blood sugar levels.",
                            ExpireDate = new DateTime(2023, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ImagePath = "default.png",
                            ImportDate = new DateTime(2022, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ManufacturedDate = new DateTime(2023, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Manufacturer = "DMP",
                            Price = 10000f,
                            ProductName = "Metformin ",
                            ProductTypeID = 2,
                            Quantity = 100
                        },
                        new
                        {
                            ProductID = 6,
                            CategoryID = 2,
                            Description = "a hollow, cylinder-shaped piece of equipment used for sucking liquid out of something or pushing liquid into something, especially one with a needle that can be put under the skin and used to inject drugs, remove small amounts of blood, etc.",
                            ImagePath = "default.png",
                            ImportDate = new DateTime(2022, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ManufacturedDate = new DateTime(2023, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Manufacturer = "DMP",
                            Price = 10000f,
                            ProductName = "Syringe",
                            ProductTypeID = 3,
                            Quantity = 100
                        },
                        new
                        {
                            ProductID = 7,
                            CategoryID = 2,
                            Description = "a device used for measuring temperature, especially of the air or in a person's body.",
                            ImagePath = "default.png",
                            ImportDate = new DateTime(2022, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ManufacturedDate = new DateTime(2023, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Manufacturer = "DMP",
                            Price = 10000f,
                            ProductName = "Thermometer",
                            ProductTypeID = 3,
                            Quantity = 100
                        },
                        new
                        {
                            ProductID = 8,
                            CategoryID = 2,
                            Description = "a small solid piece of medicine that a person swallows without chewing",
                            ImagePath = "default.png",
                            ImportDate = new DateTime(2022, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ManufacturedDate = new DateTime(2023, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Manufacturer = "DMP",
                            Price = 10000f,
                            ProductName = "Pill",
                            ProductTypeID = 3,
                            Quantity = 100
                        },
                        new
                        {
                            ProductID = 9,
                            CategoryID = 2,
                            Description = "a small piece of equipment made of two narrow strips of metal joined at one end. It is used to pull out hairs or to pick up small objects by pressing the two strips of metal together with the fingers",
                            ImagePath = "default.png",
                            ImportDate = new DateTime(2022, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ManufacturedDate = new DateTime(2023, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Manufacturer = "DMP",
                            Price = 10000f,
                            ProductName = "Tweezers",
                            ProductTypeID = 3,
                            Quantity = 100
                        },
                        new
                        {
                            ProductID = 10,
                            CategoryID = 2,
                            Description = "a piece of medical equipment that doctors use to listen to your heart and lungs",
                            ImagePath = "default.png",
                            ImportDate = new DateTime(2022, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            ManufacturedDate = new DateTime(2023, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Manufacturer = "DMP",
                            Price = 10000f,
                            ProductName = "Stethoscope",
                            ProductTypeID = 3,
                            Quantity = 100
                        });
                });

            modelBuilder.Entity("EProjectGroup2.Models.ProductTypes", b =>
                {
                    b.Property<int>("ProductTypeID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("ProductTypeID"), 1L, 1);

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ProductName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ProductTypeID");

                    b.ToTable("ProductTypes");

                    b.HasData(
                        new
                        {
                            ProductTypeID = 1,
                            Description = "Antipyretics block or reverse fever's cytokine-mediated rise in core temperature, but do not affect body temperature in the afebrile state.",
                            ProductName = "Antipyretics"
                        },
                        new
                        {
                            ProductTypeID = 2,
                            Description = "An analgesic drug, also called simply an analgesic (American English), analgaesic (British English), pain reliever, or painkiller, is any member of the group of drugs used to achieve relief from pain (that is, analgesia or pain management).",
                            ProductName = "Analgesic"
                        },
                        new
                        {
                            ProductTypeID = 3,
                            Description = "A medical device is any device intended to be used for medical purposes. Significant potential for hazards are inherent when using a device for medical purposes and thus medical devices must be proved safe and effective with reasonable assurance before regulating governments allow marketing of the device in their country. As a general rule, as the associated risk of the device increases the amount of testing required to establish safety and efficacy also increases. Further, as associated risk increases the potential benefit to the patient must also increase.",
                            ProductName = "Medical Device"
                        });
                });

            modelBuilder.Entity("EProjectGroup2.Models.UserFunction", b =>
                {
                    b.Property<int>("UserID")
                        .HasColumnType("int");

                    b.Property<int>("FunctionID")
                        .HasColumnType("int");

                    b.HasKey("UserID", "FunctionID");

                    b.HasIndex("FunctionID");

                    b.ToTable("UserFunction");
                });

            modelBuilder.Entity("EProjectGroup2.Models.UserGroup", b =>
                {
                    b.Property<int>("UserID")
                        .HasColumnType("int");

                    b.Property<int>("GroupID")
                        .HasColumnType("int");

                    b.HasKey("UserID", "GroupID");

                    b.HasIndex("GroupID");

                    b.ToTable("UserGroup");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Users", b =>
                {
                    b.Property<int>("UserID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("UserID"), 1L, 1);

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FullName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Password")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserID");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            UserID = 1,
                            Email = "admin@admin",
                            FullName = "admin",
                            Password = "admin",
                            PhoneNumber = "1234",
                            UserName = "admin"
                        });
                });

            modelBuilder.Entity("EProjectGroup2.Models.Address", b =>
                {
                    b.HasOne("EProjectGroup2.Models.Customers", "Customers")
                        .WithMany("Address")
                        .HasForeignKey("CustomerID");

                    b.Navigation("Customers");
                });

            modelBuilder.Entity("EProjectGroup2.Models.CartDetails", b =>
                {
                    b.HasOne("EProjectGroup2.Models.Carts", "Carts")
                        .WithMany("CartDetails")
                        .HasForeignKey("CartID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EProjectGroup2.Models.Products", "Products")
                        .WithMany("CartDetails")
                        .HasForeignKey("ProductID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Carts");

                    b.Navigation("Products");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Customers", b =>
                {
                    b.HasOne("EProjectGroup2.Models.Carts", "Carts")
                        .WithOne("Customers")
                        .HasForeignKey("EProjectGroup2.Models.Customers", "CartID");

                    b.Navigation("Carts");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Educations", b =>
                {
                    b.HasOne("EProjectGroup2.Models.Users", "Users")
                        .WithMany("Educations")
                        .HasForeignKey("UserID");

                    b.Navigation("Users");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Feedbacks", b =>
                {
                    b.HasOne("EProjectGroup2.Models.Products", "Products")
                        .WithMany("Feedbacks")
                        .HasForeignKey("ProductID");

                    b.Navigation("Products");
                });

            modelBuilder.Entity("EProjectGroup2.Models.FunctionGroup", b =>
                {
                    b.HasOne("EProjectGroup2.Models.Functions", "Functions")
                        .WithMany("FunctionGroups")
                        .HasForeignKey("FunctionID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EProjectGroup2.Models.Groups", "Groups")
                        .WithMany("FunctionGroups")
                        .HasForeignKey("GroupID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Functions");

                    b.Navigation("Groups");
                });

            modelBuilder.Entity("EProjectGroup2.Models.OrderDetails", b =>
                {
                    b.HasOne("EProjectGroup2.Models.Orders", "Orders")
                        .WithMany("OrderDetails")
                        .HasForeignKey("OrderID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EProjectGroup2.Models.Products", "Products")
                        .WithMany("OrderDetails")
                        .HasForeignKey("ProductID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Orders");

                    b.Navigation("Products");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Orders", b =>
                {
                    b.HasOne("EProjectGroup2.Models.Address", "Address")
                        .WithMany("Orders")
                        .HasForeignKey("AddressID");

                    b.HasOne("EProjectGroup2.Models.Customers", "Customers")
                        .WithMany("Orders")
                        .HasForeignKey("CustomerID");

                    b.HasOne("EProjectGroup2.Models.Users", "Users")
                        .WithMany("Orders")
                        .HasForeignKey("UserID");

                    b.Navigation("Address");

                    b.Navigation("Customers");

                    b.Navigation("Users");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Products", b =>
                {
                    b.HasOne("EProjectGroup2.Models.Categories", "Categories")
                        .WithMany("Products")
                        .HasForeignKey("CategoryID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EProjectGroup2.Models.ProductTypes", "ProductTypes")
                        .WithMany("Products")
                        .HasForeignKey("ProductTypeID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Categories");

                    b.Navigation("ProductTypes");
                });

            modelBuilder.Entity("EProjectGroup2.Models.UserFunction", b =>
                {
                    b.HasOne("EProjectGroup2.Models.Functions", "Functions")
                        .WithMany("UserFunctions")
                        .HasForeignKey("FunctionID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EProjectGroup2.Models.Users", "Users")
                        .WithMany("UserFunctions")
                        .HasForeignKey("UserID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Functions");

                    b.Navigation("Users");
                });

            modelBuilder.Entity("EProjectGroup2.Models.UserGroup", b =>
                {
                    b.HasOne("EProjectGroup2.Models.Groups", "Groups")
                        .WithMany("UserGroups")
                        .HasForeignKey("GroupID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EProjectGroup2.Models.Users", "Users")
                        .WithMany("UserGroups")
                        .HasForeignKey("UserID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Groups");

                    b.Navigation("Users");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Address", b =>
                {
                    b.Navigation("Orders");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Carts", b =>
                {
                    b.Navigation("CartDetails");

                    b.Navigation("Customers");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Categories", b =>
                {
                    b.Navigation("Products");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Customers", b =>
                {
                    b.Navigation("Address");

                    b.Navigation("Orders");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Functions", b =>
                {
                    b.Navigation("FunctionGroups");

                    b.Navigation("UserFunctions");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Groups", b =>
                {
                    b.Navigation("FunctionGroups");

                    b.Navigation("UserGroups");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Orders", b =>
                {
                    b.Navigation("OrderDetails");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Products", b =>
                {
                    b.Navigation("CartDetails");

                    b.Navigation("Feedbacks");

                    b.Navigation("OrderDetails");
                });

            modelBuilder.Entity("EProjectGroup2.Models.ProductTypes", b =>
                {
                    b.Navigation("Products");
                });

            modelBuilder.Entity("EProjectGroup2.Models.Users", b =>
                {
                    b.Navigation("Educations");

                    b.Navigation("Orders");

                    b.Navigation("UserFunctions");

                    b.Navigation("UserGroups");
                });
#pragma warning restore 612, 618
        }
    }
}
